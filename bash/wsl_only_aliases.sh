############
# WSL ONLY #
############

# Copy file contents to clipboard
clip() {
  cat "$1" | clip.exe
}

# Open Windows Explorer in current directory
alias explore='explorer.exe .'
