# see README.md for instructions

# Copy prompt re-definition, preserve metadata, update (copy only if missing)
sudo cp -p -u ./bash_prompt.sh /root/.bash_prompt

# Copy aliases, preserve metadata, update (copy only if missing)
sudo cp -p -u ./bash_aliases.sh /root/.bash_aliases

echo OK
