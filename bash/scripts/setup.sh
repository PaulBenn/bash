# see README.md for instructions

# Copy prompt re-definition, preserve metadata, update (copy only if missing)
cp -p -u ./bash_prompt.sh "$HOME/.bash_prompt"

# Copy aliases, preserve metadata, update (copy only if missing)
cp -p -u ./bash_aliases.sh "$HOME/.bash_aliases"

echo OK
