# Bash set-up
Set up `~/.bash_aliases` and `~/.bash_prompt` for **current** user:
```shell
./bash/scripts/setup.sh
```

Set up `~/.bash_aliases1` and `~/.bash_prompt` for **root** user:
```shell
sudo ./bash/scripts/setup_root.sh
```

Edit `~/.bashrc` or `/root/.bashrc` as appropriate, to make sure both files load when the terminal does:

```shell
# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -e $HOME/.bash_aliases ]; then
    source $HOME/.bash_aliases
fi

# Command prompt definition.
if [ -e $HOME/.bash_prompt ]; then
    source $HOME/.bash_prompt
fi
```

### Other useful commands
- Change `git push`'s default behaviour to assume that the upstream branch is supposed to have the same name as the local branch (allows the use of `git push -u` without forcing the use of `--set-upstream-to`):

    ```shell
    git config --global push.default current
    ```
