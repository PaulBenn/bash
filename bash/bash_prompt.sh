# Custom prompt configuration (via PS1 environment variable)

# return current Git branch
function git_branch_status() {
	git_branch=$(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
	if [ ! "${git_branch}" == "" ]
	then
		echo " [${git_branch}]"
	else
		echo ""
	fi
}

export PS1="\[\e[31m\]\\$\[\e[m\] [\[\e[33m\]\t\[\e[m\]] \[\e[36m\]\w\[\e[m\]\[\e[32m\]\`git_branch_status\`\[\e[m\] "
