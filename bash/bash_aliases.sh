# Custom Bash aliases

# log out
alias logout='gnome-session-quit --no-prompt'

# open current directory (GUI file explorer)
alias explore='gio open .'

# switch to root
alias root='sudo su root -'

# switch to me
alias me='sudo su paul -'

# cd up a directory
alias ..='cd ..'

# cd to ~
alias home='cd ~'

# cd to ~/repos
alias repos='cd ~/repos'

# ls
# -a = all
# -l = list
# -h = human-readable sizes
# -F = append "/" to directories
alias ls='ls -alhF --color=auto'

# make directories recursively, print verbose
alias mkdir='mkdir -p -v'

# vi -> vim
alias vi=vim
alias svi='sudo vi'
alias vis='vim "+set si"'
alias edit='vim'

# show formatted $PATH
alias path='echo -e ${PATH//:/\\n}'

# grep command history
alias gh='history | grep'

# count files in a directory
alias count='find . -type f | wc -l'

# Memory info
alias meminfo='free -m -l -t'

# CPU info
alias cpuinfo='lscpu'

# Git: cd to top level, switch to master branch and update
alias master='cd `git rev-parse --show-toplevel` && git checkout master && git pull'
# Git: cd to top level, switch to main branch and update
alias main='cd `git rev-parse --show-toplevel` && git checkout main && git pull'

# copy verbose
# -a = archive mode: copy recursive, copy all symlinks and file attributes, allow copying devices and sockets
# -h = output numbers in a human-readable format
# --info=progress2 = show progress bar (progress1: per-file, progress2: total)
alias cpv='rsync -ah --info=progress2'

# IDEA
alias idea='nohup idea </dev/null >/dev/null 2>&1 &'

# --- FUNCTIONS ----------------------------

# Find PID by port
pidport() {
  sudo ss -lptn "sport = :$1"
}

# Find largest N files in current directory (N = 10 by default)
largest() {
  find . -type f -exec du -a -BM {} \+ | sort -n -r | head -n "${1:-10}" | more
}

# Git: undo N changes (N = 1 by default)
undo() {
  git reset "HEAD~${1:-1}" --soft
}

# --- UnlikelyAI-specific aliases ----------

# Connect to VPN
alias vpn='nmcli connection up paul-unlikelyai'
# Disconnect from VPN
alias vpndown='nmcli connection down paul-unlikelyai'

