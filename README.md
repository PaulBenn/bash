# Set-up and tooling for new Ubuntu installations

## OS Installation
1. Find a USB drive with 8 GB capacity or greater, and remove all existing data from it.
2. Boot up the existing Windows operating system.
3. Download the latest Ubuntu Desktop ISO image from the [Ubuntu releases page](https://releases.ubuntu.com/).
4. Download and install [Rufus](https://rufus.ie/en/).
5. Open Rufus and burn the ISO image onto the USB drive. Make sure to select the correct partition scheme: usually GPT.
6. This is the last time you will use Windows on the computer. Make back-ups of anything you need for Ubuntu.
7. Reboot. Interrupt the normal boot sequence (`Enter` on Lenovo). Enter the boot device selection menu.
8. Boot from the USB drive, and allow some time for the bootstrap OS to load.
9. Select "Install Ubuntu" (not "Try Ubuntu") and follow the installation steps.
10. When you reach "Installation type", select "Erase disk and install Ubuntu". Click "Advanced features...".
11. Select "Use LVM with the new Ubuntu installation". Tick "Encrypt the new Ubuntu installation for security".
12. Proceed. Set a security key and a recovery key, which Ubuntu will use to encrypt your disk.
13. **Save these keys immediately. You will not be able to recover your data without the encryption keys.**
14. Follow the remaining installation steps. The system will reboot automatically.
15. The system should prompt you for your disk encryption key on boot. Enter it.
16. After this, the regular Ubuntu log-in screen will appear. Enter the credentials you set up during installation.
17. Your system will auto-encrypt itself on shutdown, but not on suspend, or on log-out.

Welcome to Linux. Now, follow the steps below.

## Post-installation steps
Update:
```shell
sudo apt-get update
```

Maximise the performance of the OS:
- Settings > Power > Power Mode
- Set to "Performance"

Disable the OS function that blanks the screen after a period of inactivity:
- Settings > Power > Screen Blank
- Set to "Never"

Remove unused applications:
- Games installed by default
- Cheese

Edit the taskbar to your liking:
- Right-click any default taskbar icon > Remove from Favourites
- Right-click the icon of any open application > Add to Favourites
- File Explorer, Terminal, Slack and IDEA are a good starting set of favourites

If you want a Windows-like desktop, install it:
- Install GNOME Tweaks and the GNOME shell extensions
    ```shell
    sudo apt install gnome-tweaks gnome-shell-extensions
    ```
- Install the [Dash to Panel](https://extensions.gnome.org/extension/1160/dash-to-panel/) extension

If you need to type accented letters, enable the [Compose key](https://help.ubuntu.com/community/ComposeKey):
- Settings > Keyboard > Special Character Entry > Compose Key
- Set to `Right Ctrl`

Disable calendar window pop-ups:
```shell
gsettings set org.gnome.evolution-data-server.calendar notify-with-tray true
```

### Bash set-up
Set up `~/.bash_aliases` and `~/.bash_prompt` for **current** user:
```shell
./bash/scripts/setup.sh
```

Set up `~/.bash_aliases1` and `~/.bash_prompt` for **root** user:
```shell
sudo ./bash/scripts/setup_root.sh
```

Edit `~/.bashrc` or `/root/.bashrc` as appropriate, to make sure both files load when the terminal does:

```shell
# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -e $HOME/.bash_aliases ]; then
    source $HOME/.bash_aliases
fi

# Command prompt definition.
if [ -e $HOME/.bash_prompt ]; then
    source $HOME/.bash_prompt
fi
```

### Essential packages
Better swap management:
```shell
sudo apt install zram-config
# (restart)
```

`curl`:
```shell
sudo apt install curl
vim ~/.curlrc
# add one line as follows to automatically print a new line at the end of response bodies:
# -w "\n"
```

Docker (see also [official guide](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)):
```shell
sudo apt-get install ca-certificates gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose docker-compose-plugin
sudo systemctl enable docker.service
sudo usermod -aG docker $USER
newgrp docker
# Verify:
sudo service docker start
sudo docker run hello-world
```

`locate` command:
```shell
sudo apt install plocate
```

[neovim](https://neovim.io/) (better `vim`):
```shell
sudo apt install neovim
```

[Sublime Text](https://www.sublimetext.com/) editor:
```shell
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
```

[nvm](https://github.com/nvm-sh/nvm/) (NodeJS version manager):
```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
source ~/.bashrc
```

[sdkman](https://sdkman.io/) (Software Development Kit Manager):
```shell
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
```

[Java](https://www.java.com/en/), [Maven](https://maven.apache.org/), [Gradle](https://gradle.org/) (requires `sdk` from `sdkman` above):
```shell
# (pick appropriate version e.g. '17')
sdk install java x.y.z-open
sdk install maven
sdk install gradle
# and, if needed, Java Mission Control:
sdk install jmc
```

[`pyenv`](https://github.com/pyenv/pyenv/) (Python version manager) + [Python](https://www.python.org/):
```shell
curl https://pyenv.run | bash
# (pick appropriate version e.g. '3.14')
pyenv install x.y.z
```

[pre-commit](https://pre-commit.com/) (Git commit hooks):
```shell
sudo apt install pre-commit
```

[Amazon Web Services CLI](https://aws.amazon.com/cli/):
```shell
sudo apt install awscli
aws configure
```

[Google Chrome](https://www.google.com/intl/en_uk/chrome/) (browser):
- Go to the [Chrome downloads page](https://www.google.com/chrome/?platform=linux), and click "Download Chrome"
- Install via `apt`:
    ```shell
    sudo apt install google-chrome-stable
    ```

[Insomnia](https://insomnia.rest/) (HTTP request manager):
- Go to the [Insomnia downloads page](https://insomnia.rest/download), and click "Download Insomnia for Ubuntu"
- Install via `apt`:
    ```shell
    sudo apt install ./Insomnia.Core-x.y.z.deb
    ```

### SSH set-up
Set up public/private key pairs:
- Check your existing keys: [GitLab](https://gitlab.com/-/profile/keys), [GitHub](https://github.com/settings/keys)
- Direct links to [GitLab's guide](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair) and [GitHub's guide](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) on the topic
- Generally, it's enough to:
    - Generate a key:
        ```shell
        ssh-keygen -t ed25519 -C "<email>"
        ```
    - Add it to the SSH agent:
        ```shell
        eval "$(ssh-agent -s)"
        ```

### Git set-up
Required configuration:
```shell
git config --global user.email "<email>"
git config --global user.name "Paul Benn"
```

Change default push behaviour: assume the upstream branch has the same name as the local branch. Allows the use of `git push -u` without forcing the use of `--set-upstream-to`.
```shell
git config --global push.default current
```

Install Git LFS:
```shell
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
git lfs install
```

### IntelliJ IDEA set-up
Install `libfuse2` (required for JetBrains Toolbox):
```shell
sudo apt install libfuse2
```

Install the [JetBrains Toolbox](https://www.jetbrains.com/toolbox-app/):
```shell
curl -fsSL https://raw.githubusercontent.com/nagygergo/jetbrains-toolbox-install/master/jetbrains-toolbox.sh | bash
```

Install and run IntelliJ IDEA:
- Open JetBrains Toolbox and install IntelliJ IDEA.
- Start IDEA via the `idea` alias (`nohup idea </dev/null >/dev/null 2>&1 &`). This ensures the IDE inherits environment variables from the shell (important for certain `pre-commit` hooks to work correctly, among other things).

Disable default `Ctrl+Alt+Left` and `Ctrl+Alt+Right` shortcuts:
```shell
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "[]"
```

Bind `Ctrl+Alt+Left` and `Ctrl+Alt+Right` to editor navigation actions:
- Settings > Keymap > GNOME > Main Menu > Navigate > {Back|Forward}
- remove existing shortcuts
- set "Back" to `Ctrl+Alt+Left`, "Forward" to `Ctrl+Alt+Right`

Disable unused plugins:
- Help > Find Action... > Plugins > Installed
- Kotlin, Android, and Smali are potential candidates.

Add relevant plugins:
- Help > Find Action... > Plugins > Marketplace
- Python, Terraform, AWS Tool-Kit, Key Promoter X, and Nyan Progress Bar are potential candidates.

### KeePass set-up
Install `rclone`:
```shell
sudo -v ; curl https://rclone.org/install.sh | sudo bash
```

Modify `fusermount` to allow non-root user mounts:
```shell
sudo vim /etc/fuse.conf
# (uncomment user_allow_other, save and exit)
```

Configure `rclone` (see `rclone/rclone.conf` for guidance - secrets available in KeePass):
```shell
rclone config 
```

Create mount point:
```shell
mkdir -p ~/google-drive-pb
```

Create service unit:
```shell
sudo vim /etc/systemd/system/google-drive-rclone-mount.service
# (copy contents of rclone/google-drive-rclone-mount.service)
```

Enable the service (set it to start on system start-up):
```shell
sudo service google-drive-rclone-mount enable
```

Start the service (and check it ran successfully):
```shell
sudo service google-drive-rclone-mount start
```

Install [KeePassXC](https://keepassxc.org/):
```shell
sudo apt install keepassxc
```

Set up [KeePassXC-Browser](https://chrome.google.com/webstore/detail/keepassxc-browser/oboonakemofpalcgghocfoadofidjkkk/):
- Install the extension from the link above
- Open KeePassXC, then open and unlock the database under `~/google-drive-pb`
- Enable browser integration: Tools > Settings > Browser Integration > Enable browser integration
- Change browser integration settings:
  - tick "General > Search in all opened databases for matching credentials"
  - tick "Advanced > Never ask before accessing credentials"
- Go to browser, click extension, follow set-up prompts to enable the use of the unlocked database
